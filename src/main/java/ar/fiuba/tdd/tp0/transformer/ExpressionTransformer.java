package ar.fiuba.tdd.tp0.transformer;

import java.util.Stack;

/**
 * Created by nico on 07/09/15.
 */
public interface ExpressionTransformer {

    void operate(String values);
}
