package ar.fiuba.tdd.tp0;

import ar.fiuba.tdd.tp0.transformer.ExpressionTransformer;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class RPNCalculator {

    private Map<String, ExpressionTransformer> mathExpressions;
    private Stack<Float> result;

    public RPNCalculator() {
        this.result = new Stack<>();
        this.initOperationsMap();
    }

    public float eval(String expression) {

        String[] values = this.getValues(expression);
        for (int i = 0; i < values.length; i++) {
            ExpressionTransformer op;
            op = this.getMathExpressions().getOrDefault(values[i], (String value) -> {
                    this.getResult().push(Float.parseFloat(value));
                });
            op.operate(values[i]);
        }

        Float finalValue = Float.valueOf(1);
        while (!this.getResult().empty()) {
            finalValue = this.getResult().pop();
        }

        return finalValue;
    }

    private Map<String, ExpressionTransformer> initOperationsMap() {

        this.mathExpressions = new HashMap<>();
        this.initSimpleOperationsMap();
        this.initMultipleOperationsMap();

        return mathExpressions;
    }

    private void initSimpleOperationsMap() {

        mathExpressions.put("+", term -> { this.getResult().push(this.getResult().pop() + this.getResult().pop()); });
        mathExpressions.put("-", term -> {
                Float term1  = this.getResult().pop();
                Float term2  = this.getResult().pop();

                this.getResult().push(term2 - term1);
            });

        mathExpressions.put("*", term -> this.getResult().push(this.getResult().pop() * this.getResult().pop()));

        mathExpressions.put("MOD", term -> {
                Float term1  = this.getResult().pop();
                Float term2 = this.getResult().pop();

                this.getResult().push(term2 % term1);
            });

        mathExpressions.put("/", term -> {
                Float term1  = this.getResult().pop();
                Float term2 = this.getResult().pop();

                this.getResult().push(term2 / term1);
            });

    }

    private void initMultipleOperationsMap() {

        initMultipleOperationsSumSustract();
        initMultipleOperationsMultiplyDivide();
    }

    private void initMultipleOperationsSumSustract() {
        mathExpressions.put("++", term -> {
                while (this.getResult().size() > 1) {
                    this.getResult().push(this.getResult().pop() + this.getResult().pop());
                }
            });

        mathExpressions.put("--", term -> {
                float total = this.getResult().pop();
                while (this.getResult().size() > 0) {
                    total -= this.getResult().pop();
                }
                this.getResult().push(total);
            });

    }

    private void initMultipleOperationsMultiplyDivide() {

        mathExpressions.put("//", term -> {
                float total = this.getResult().pop();
                while (this.getResult().size() > 0) {
                    total /= this.getResult().pop();
                }
                this.getResult().push(total);
            });

        mathExpressions.put("**", (term) -> {
                float total = this.getResult().pop();
                while (this.getResult().size() > 0) {
                    total *= this.getResult().pop();
                }
                this.getResult().push(total);
            });

    }


    protected Map<String, ExpressionTransformer> getMathExpressions() {
        return mathExpressions;
    }

    public Stack<Float> getResult() {
        return result;
    }

    private String[] getValues(String expression) throws IllegalArgumentException {
        //FIXME No me salio esta verificacion sin romper las reglas :'(
        try {
            String[] values = expression.split(" ");
            //Chequeamos que hayan al menos 2 valores , o bien, un valor y un operador multiple
            Float.parseFloat(values[0]);
            if (values[1].length() < 2  && values.length > 1) {
                Float.parseFloat(values[1]);
            }
            return values;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }

    }
}
