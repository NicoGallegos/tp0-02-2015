package ar.fiuba.tdd.tp0.operations;

/**
 * Created by nico on 09/09/15.
 * No se utiliza porque de lo contrario falla build por duplicate code. Verificar
 */
public class Operations {


    public static Float sum(Float term2, Float term1) {
        return (term1 + term2);
    }

    public static Float subtract(Float term2, Float term1) {
        return (term1 - term2);
    }


    public static Float multiply(Float term2, Float term1) {
        return term1 * term2;
    }

    public static Float mod(Float term2, Float term1) {
        return term1 % term2;
    }

    public static Float division(Float term2, Float term1) {
        return term1 / term2;
    }

}

